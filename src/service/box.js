var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
var reqGet = {
  method: "GET",
  headers: myHeaders,
  body: JSON.stringify(),
  redirect: "follow",
};

export const getallBox = async () => {
  const res = await fetch(
    "https://api.jsonbin.io/b/5f3154b06f8e4e3faf2f99de",
    reqGet
  );
  const list = await res.json();
  return list;
};

export const saveBox = async (boxItem) => {
  const res = await fetch("http://localhost:7000/post", {
    method: "POST",
    headers: myHeaders,
    body: JSON.stringify(boxItem),
    redirect: "follow",
  });
};

export const DeleteBox = (id) => {
  fetch("http://localhost:7000/delete_box/" + id, {
    method: "DELETE",
    headers: myHeaders,
    redirect: "follow",
  })
    .then((response) => response.text())
    .then((result) => console.log("delete complete"))
    .catch((error) => console.log("error", error));
};

export const UpdateBox = async (Id, boxItem) => {
  const res = await fetch("http://localhost:7000/update_box/" + Id, {
    method: "PUT",
    headers: myHeaders,
    body: JSON.stringify(boxItem),
    redirect: "follow",
  });
};
