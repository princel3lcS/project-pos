import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import Nav from "react-bootstrap/Nav";
import NavBar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import post from "./component/post";
import list from "./component/list";
import edit from "./component/edit";


export default function App() {
  return (
    <div>
      <Route>
        <div>
          <NavBar bg="dark" variant="dark">
            <Container>
              <NavBar.Brand>
                <Link to="/post" className="nav-link">
                  MERN
                </Link>
              </NavBar.Brand>
              <Nav className="justify-content-end">
                <Nav>
                  <Link to="/post" className="nav-link">
                    Post
                  </Link>
                </Nav>
                <Nav>
                  <Link to="/list" className="nav-link">
                    List
                  </Link>
                </Nav>
              </Nav>
            </Container>
          </NavBar>
          <Container>
            <Row>
              <Col>
                <div className="wrapper">
                  <Switch>
                    <Route exact path="/" component={post} />
                    <Route path="/list" component={list} />
                    <Route path="/post" component={post} />
                    <Route path="/edit/:id" component={edit} />
                  </Switch>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </Route>
    </div>
  );
}
